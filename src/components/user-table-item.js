import React from 'react'
import img_user_default from '../images/square-image.png'
import '../css/user-table.css'


const UserTableItem = ({ user }) => {
  return (
    <tr>
      <td>
        <h4 className="ui image header">
          {
            user.photo ?
              <img className="ui small rounded image" src={user.photo} alt="" />
              :
              <img className="ui small rounded image" src={img_user_default} alt="" />
          }
          <div className="content">
            <p>{user.name}</p>
            <div className="sub header">
              {user.email.toString()}
            </div>
          </div>
        </h4>
      </td>
      <td>
        {user.password.toString()}
      </td>
    </tr >
  );
}

export default UserTableItem;