import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import UsersTable from './components/users-table'
import { DB_CONFIG } from './config/config'
import firebase from 'firebase/app'
import firebaseStorage from 'firebase'
import 'firebase/database'
import pickImage from './images/square-image.png'
import FileReaderInput from 'react-file-reader-input'
import EncodeBase64 from 'base64-arraybuffer'
import ImageBase64 from './utils/ImageBase64.js'

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {

      name: 'iago',
      email: 'iago@gmail.com',
      password: '111111',
      confirm_password: '111111',

      label_foto: 'Adicionar',
      imageData: null,

      users: [],

      loaded: true,

      name_validate: '',
      email_validate: '',
      password_validate: '',
      confirm_password_validate: '',
    }

    this.app = firebase.initializeApp(DB_CONFIG);
    this.db = this.app.database().ref().child('users');

    this.onHandleInputChange = this.onHandleInputChange.bind(this);
    this.onHandleSubmit = this.onHandleSubmit.bind(this);
    this.onChangeImage = this.onChangeImage.bind(this);
    this.clearForm = this.clearForm.bind(this);
    this.upload = ''

    this.validate = this.validate.bind(this);

  }

  componentWillMount() {

    const users_now = this.state.users;

    this.db.on('child_added', snap => {
      users_now.push({
        key: snap.key,
        name: snap.val().name,
        email: snap.val().email,
        password: snap.val().password,
        photo: snap.val().photo
      })

      this.setState({
        users: users_now
      })

    })

    let stateFull = this;

    this.db.on('child_changed', function (data) {

      users_now.map((user) => {
        if (user.key == data.key) {

          console.log("Foto: " + data.val().photo);
          user.photo = data.val().photo;

          console.log(user);


        }
      });

      stateFull.forceUpdate();
    });

  }

  onHandleInputChange(event) {

    //Obtendo nome do atributo
    let attName = event.target.name;

    //Obtendo nome da validacao do atributo
    let attNameValidate = event.target.name + "_validate";
    if (attNameValidate === "password_validate") {

      this.setState({
        [attName]: event.target.value,
        [attNameValidate]: '',
        confirm_password_validate: '',
      })

    } else {

      this.setState({
        [attName]: event.target.value,
        [attNameValidate]: '',
      })
    }

  }

  onChangeImage(event, results) {
    results.forEach(result => {
      const [event, file] = result;
      const imageData = EncodeBase64.encode(event.target.result)
      this.setState({
        label_foto: "Alterar",
        imageData: imageData
      })
    });
  }

  onHandleSubmit(event) {
    event.preventDefault();

    this.setState({
      loaded: false
    })

    if (this.validate()) {

      console.log(
        "name: " + this.state.name +
        "\nemail: " + this.state.email +
        "\nsenha: " + this.state.password
      )

      this.db
        .push({
          name: this.state.name,
          email: this.state.email,
          password: this.state.password,
        })
        .then((response) => {
          console.log(response.key)

          let img = this.state.imageData

          this.clearForm();

          if (img) {

            firebaseStorage.storage()
              .ref("/photos/" + response.key + "/")
              .putString(img, 'base64')
              .then((url) => {
                firebase.database().ref(`users/${response.key}`)
                  .update({ photo: url.downloadURL })
              })
          }
        })

    } else {
      this.setState({
        loaded: true,
      })
    }

  }

  clearForm() {

    this.setState({
      name: '',
      email: '',
      password: '',
      confirm_password: '',

      label_foto: 'Adicionar',
      imageData: null,

      name_validate: '',
      email_validate: '',
      password_validate: '',
      confirm_password_validate: '',

      loaded: true,
    })

  }

  validate() {

    console.log("Validando...")

    const email_filter = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/

    let error;

    if (!this.state.name) {
      this.setState({
        name_validate: "Insira um nome"
      })
      error = true;
    }

    if (!this.state.email) {
      this.setState({
        email_validate: "Insira um email"
      })
      error = true;
    } else if (!email_filter.test(this.state.email)) {
      this.setState({
        email_validate: "Email inválido"
      })
      error = true;
    }

    if (!this.state.password) {
      this.setState({
        password_validate: "Insira uma senha"
      })
      error = true;
    } else if (this.state.password.length < 6) {
      this.setState({
        password_validate: "A senha deve ter no mínimo 6 dígitos"
      })
      error = true;
    }

    if (this.state.confirm_password != this.state.password) {
      this.setState({
        confirm_password_validate: "A confirmação de senha deve ser igual a senha"
      })
      error = true;
    }

    return !error;
  }

  render() {
    return (
      <div className="App">
        <div className="container">

          <h1>React Power</h1>

          <form className="ui form" onSubmit={(event) => this.onHandleSubmit(event)}>
            <div className="field">
              <label>Nome</label>
              <input type="text" name="name" placeholder="Nome" onChange={this.onHandleInputChange}
                value={this.state.name} />
              {
                this.state.name_validate !== '' ?
                  <p className="ui red bottom attached mini message">{this.state.name_validate}</p> : null
              }
            </div>
            <div className="field">
              <label>Email</label>
              <input type="text" name="email" placeholder="Email" onChange={this.onHandleInputChange}
                value={this.state.email} />
              {
                this.state.email_validate !== '' ?
                  <p className="ui red bottom attached mini message">{this.state.email_validate}</p> : null
              }
            </div>
            <div className="field">
              <label>Senha</label>
              <input type="password" name="password" placeholder="Senha" onChange={this.onHandleInputChange}
                value={this.state.password} />
              {
                this.state.password_validate !== '' ?
                  <p className="ui red bottom attached mini message">{this.state.password_validate}</p> : null
              }
            </div>
            <div className="field">
              <label>Confirmar senha</label>
              <input type="password" name="confirm_password" placeholder="Confirmar senha" onChange={this.onHandleInputChange}
                value={this.state.confirm_password} />
              {
                this.state.confirm_password_validate !== '' ?
                  <p className="ui red bottom attached mini message">{this.state.confirm_password_validate}</p> : null
              }
            </div>
            <div className="fields">
              <div className="field">
                <label>Foto</label>
                <div className="ui small fade rounded image container-img-logomarca">
                  <p className="labelAddFoto">{this.state.label_foto} foto</p>
                  <FileReaderInput
                    as="buffer"
                    id="my-file-input"
                    onChange={this.onChangeImage} >
                    {this.state.imageData ?
                      <ImageBase64 data={this.state.imageData} /> :
                      <img className="visible content" src={pickImage} />}
                  </FileReaderInput>
                </div>
              </div>
            </div>

            <button className="ui button" type="submit">Cadastrar</button>

            {
              !this.state.loaded ?
                <div className="ui active inverted dimmer">
                  <div className="ui loader"></div>
                </div>
                :
                null
            }
          </form>

          <div className="conatiner-table">

            {this.state.users ?
              <UsersTable users={this.state.users} />
              : null
            }
          </div>
        </div>

      </div>
    );
  }
}

export default App;
