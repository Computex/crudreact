import React from 'react';


const ImageBase64 = ({ data }) => {
  const imageBase64 = `base64,${data}`

  return (
    <img src={`data:image/jpeg;${imageBase64}`} />
  )
}

export default ImageBase64