import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'semantic-ui-css/semantic.min.css';
import registerServiceWorker from './registerServiceWorker';
import bg from './images/bg.png'

let rootStyle = {
  backgroundImage: 'url(' + bg + ')',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  WebkitTransition: 'all', // note the capital 'W' here
  msTransition: 'all' // 'ms' is the only lowercase vendor prefix
};

ReactDOM.render(
  <div /*style={rootStyle}*/ className="container">
    <App />
  </div>, document.getElementById('root'));
registerServiceWorker();
